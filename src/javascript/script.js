//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js

var ready = function ( fn ) {
  // Sanity check
  if ( typeof fn !== 'function' ) return;

  // If document is already loaded, run method
  if ( document.readyState === 'interactive' || document.readyState === 'complete' ) {
    return fn();
  }

  // Otherwise, wait until document is loaded
  document.addEventListener( 'DOMContentLoaded', fn, false );
};

ready(function () {

  svg4everybody();
});